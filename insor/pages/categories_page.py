from bs4 import BeautifulSoup
from typing import Generator
import ssl
from urllib import request

from ..page import Page
from insor.pages.topic_page import TopicPage

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

class CategoriesPage(Page):
    SUBCATEGORIES_SELECTOR = '.subcategorias .box-subcat a'
    excluded_topics: list[str] = [
        'https://educativo.insor.gov.co/catdiccionario/cuerpo-humano/',
        'https://educativo.insor.gov.co/catdiccionario/ser-humano/',
        'https://educativo.insor.gov.co/catdiccionario/calificar/',
        'https://educativo.insor.gov.co/catdiccionario/comunicacion-y-afines/',
        'https://educativo.insor.gov.co/catdiccionario/cultura-sorda/',
        'https://educativo.insor.gov.co/catdiccionario/educacion/',
        'https://educativo.insor.gov.co/catdiccionario/formulas-de-cortesia/',
    ]

    def subcategories(self) -> Generator[TopicPage, any, None]:
        html = request.urlopen(self.url, context=ctx).read()
        soup = BeautifulSoup(html, 'html.parser')
        subcategories = soup.select(self.SUBCATEGORIES_SELECTOR)
        for subcategory in subcategories:
            link = subcategory.get('href')
            if link not in self.excluded_topics:
                yield TopicPage(subcategory.get('href'))
