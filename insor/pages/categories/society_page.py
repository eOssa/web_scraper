from insor.pages.categories_page import CategoriesPage

class SocietyPage(CategoriesPage):
    path = '/catdiccionario/sociedad'

    @property
    def url(self):
        return self.host + self.path
