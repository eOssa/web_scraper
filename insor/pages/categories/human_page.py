from insor.pages.categories_page import CategoriesPage

class HumanPage(CategoriesPage):
    path = '/catdiccionario/hombre'

    @property
    def url(self):
        return self.host + self.path
