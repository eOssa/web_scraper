from bs4 import BeautifulSoup
import ssl
from urllib import request

from ..page import Page

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

class WordPage(Page):
    CATEGORY_1_LABEL_SELECTOR = '.catpost .item:nth-child(1) .titu-cat-post:nth-child(1) span'
    CATEGORY_1_SELECTOR = '.catpost .item:nth-child(1) .titu-cat-post:nth-child(3) span'
    CATEGORY_2_LABEL_SELECTOR = '.catpost .item:nth-child(2) .titu-cat-post:nth-child(1) span'
    CATEGORY_2_SELECTOR = '.catpost .item:nth-child(2) .titu-cat-post:nth-child(3) span'
    CATEGORY_3_LABEL_SELECTOR = '.catpost .item:nth-child(3) .titu-cat-post:nth-child(1) span'
    CATEGORY_3_SELECTOR = '.catpost .item:nth-child(3) .titu-cat-post:nth-child(3) span'
    CATEGORY_4_LABEL_SELECTOR = '.catpost .item:nth-child(4) .titu-cat-post:nth-child(1) span'
    CATEGORY_4_SELECTOR = '.catpost .item:nth-child(4) .titu-cat-post:nth-child(3) span'
    EXAMPLE_IMAGE_SELECTOR = '.post-categoria .col-md-4:nth-child(3) img'
    EXAMPLE_SELECTOR = '.post-categoria .col-md-4:nth-child(3) .texto-normal'
    EXAMPLE_VIDEO_SELECTOR = '.post-categoria .col-md-4:nth-child(3) input'
    HAND_CONFIGURATION_1_LABEL_SELECTOR = '.catpost .item:nth-child(4) .titu-cat-post:nth-child(1) span'
    HAND_CONFIGURATION_1_SELECTOR = '.catpost .item:nth-child(4) .titu-cat-post:nth-child(3) span'
    HAND_CONFIGURATION_2_LABEL_SELECTOR = '.catpost .item:nth-child(3) .titu-cat-post:nth-child(1) span'
    HAND_CONFIGURATION_2_SELECTOR = '.catpost .item:nth-child(3) .titu-cat-post:nth-child(3) span'
    HAND_CONFIGURATION_3_LABEL_SELECTOR = '.catpost .item:nth-child(2) .titu-cat-post:nth-child(1) span'
    HAND_CONFIGURATION_3_SELECTOR = '.catpost .item:nth-child(2) .titu-cat-post:nth-child(3) span'
    HAND_CONFIGURATION_4_LABEL_SELECTOR = '.catpost .item:nth-child(1) .titu-cat-post:nth-child(1) span'
    HAND_CONFIGURATION_4_SELECTOR = '.catpost .item:nth-child(1) .titu-cat-post:nth-child(3) span'
    MEANING_IMAGE_SELECTOR = '.post-categoria .col-md-4:nth-child(2) img'
    MEANING_SELECTOR = '.post-categoria .col-md-4:nth-child(2) .texto-normal'
    MEANING_VIDEO_SELECTOR = '.post-categoria .col-md-4:nth-child(2) input'
    SIGN_IMAGE_SELECTOR = '.post-categoria .col-md-4:nth-child(1) img'
    SIGN_SELECTOR = '.post-categoria .col-md-4:nth-child(1) .texto-normal'
    SIGN_VIDEO_SELECTOR = '.post-categoria .col-md-4:nth-child(1) input'
    url: str
    soup = None

    def __init__(self, url: str):
        self.url = url

    def get_soup(self):
        if self.soup != None:
            return self.soup
        html = request.urlopen(self.url, context=ctx).read()
        self.soup = BeautifulSoup(html, 'html.parser')
        return self.soup

    @property
    def category(self) -> str:
        tag1 = self.get_soup().select_one(self.CATEGORY_1_LABEL_SELECTOR)
        if tag1 != None and tag1.get_text() == 'Temario':
            return self.get_soup().select_one(self.CATEGORY_1_SELECTOR).get_text()
        tag2 = self.get_soup().select_one(self.CATEGORY_2_LABEL_SELECTOR)
        if tag2 != None and tag2.get_text() == 'Temario':
            return self.get_soup().select_one(self.CATEGORY_2_SELECTOR).get_text()
        tag3 = self.get_soup().select_one(self.CATEGORY_3_LABEL_SELECTOR)
        if tag3 != None and tag3.get_text() == 'Temario':
            return self.get_soup().select_one(self.CATEGORY_3_SELECTOR).get_text()
        tag4 = self.get_soup().select_one(self.CATEGORY_4_LABEL_SELECTOR)
        if tag4 != None and tag4.get_text() == 'Temario':
            return self.get_soup().select_one(self.CATEGORY_4_SELECTOR).get_text()
        return ''

    @property
    def example(self) -> str:
        return self.get_soup().select_one(self.EXAMPLE_SELECTOR).get_text().strip()

    @property
    def example_image(self) -> str:
        return self.get_soup().select_one(self.EXAMPLE_IMAGE_SELECTOR).get('src')

    @property
    def example_video(self) -> str:
        return self.get_soup().select_one(self.EXAMPLE_VIDEO_SELECTOR).get('vid-cargado')

    @property
    def hand_configuration(self) -> str:
        tag1 = self.get_soup().select_one(self.HAND_CONFIGURATION_1_LABEL_SELECTOR)
        if tag1 != None and tag1.get_text() == 'Configuración Manual':
            return self.get_soup().select_one(self.HAND_CONFIGURATION_1_SELECTOR).get_text()
        tag2 = self.get_soup().select_one(self.HAND_CONFIGURATION_2_LABEL_SELECTOR)
        if tag2 != None and tag2.get_text() == 'Configuración Manual':
            return self.get_soup().select_one(self.HAND_CONFIGURATION_2_SELECTOR).get_text()
        tag3 = self.get_soup().select_one(self.HAND_CONFIGURATION_3_LABEL_SELECTOR)
        if tag3 != None and tag3.get_text() == 'Configuración Manual':
            return self.get_soup().select_one(self.HAND_CONFIGURATION_3_SELECTOR).get_text()
        tag4 = self.get_soup().select_one(self.HAND_CONFIGURATION_4_LABEL_SELECTOR)
        if tag4 != None and tag4.get_text() == 'Configuración Manual':
            return self.get_soup().select_one(self.HAND_CONFIGURATION_4_SELECTOR).get_text()
        return ''

    @property
    def meaning(self) -> str:
        return self.get_soup().select_one(self.MEANING_SELECTOR).get_text().strip()

    @property
    def meaning_image(self) -> str:
        return self.get_soup().select_one(self.MEANING_IMAGE_SELECTOR).get('src')

    @property
    def meaning_video(self) -> str:
        tag = self.get_soup().select_one(self.MEANING_VIDEO_SELECTOR)
        if tag == None:
            return ''
        return tag.get('vid-cargado')

    @property
    def sign(self) -> str:
        if self.url == 'https://educativo.insor.gov.co/diccionario/once/':
            return 'Once'
        if self.url == 'https://educativo.insor.gov.co/diccionario/uno/':
            return 'Uno'
        if self.url == 'https://educativo.insor.gov.co/diccionario/amigo/':
            return 'Amigo'
        if self.url == 'https://educativo.insor.gov.co/diccionario/casarse/':
            return 'Casarse'
        if self.url == 'https://educativo.insor.gov.co/diccionario/cunado/':
            return 'Cuñado'
        if self.url == 'https://educativo.insor.gov.co/diccionario/familia/':
            return 'Familia'
        if self.url == 'https://educativo.insor.gov.co/diccionario/guittara/':
            return 'Guitarra'
        if self.url == 'https://educativo.insor.gov.co/diccionario/primo/':
            return 'Primo'
        return self.get_soup().select_one(self.SIGN_SELECTOR).get_text().strip()

    @property
    def sign_image(self) -> str:
        return self.get_soup().select_one(self.SIGN_IMAGE_SELECTOR).get('src')

    @property
    def sign_video(self) -> str:
        return self.get_soup().select_one(self.SIGN_VIDEO_SELECTOR).get('vid-cargado')
