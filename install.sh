#!/bin/bash
if ! command -v python3 &> /dev/null; then
    echo "Python3 not installed. Installing..."
    sudo yum install python3
fi

if ! command -v mongo &> /dev/null; then
    echo "MongoDB not installed. Installing..."
    sudo cp mongodb-org-4.4.repo /etc/yum.repos.d/mongodb-org-4.4.repo
    sudo yum install -y mongodb-org
    sudo systemctl start mongod
    sudo systemctl enable mongod
fi

echo "Installing python dependencies..."
sudo pip3 install -r requirements.txt

if [ ! -f "web_scraper/.env" ]; then
    cp web_scraper/.env.example web_scraper/.env
fi

if ! command -v supervisord &> /dev/null; then
    echo "Supervisord not installed. Installing..."
    sudo yum install supervisor
    echo "Copying worker config /etc/supervisor.d/web-scraper-worker.ini"
    sudo cp web-scraper-worker.ini /etc/supervisord.d/web-scraper-worker.ini
    sudo systemctl start supervisord
    sudo systemctl enable supervisord
fi

echo "Instalation finished."
