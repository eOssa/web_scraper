import logging
import os

filename = os.path.join(os.path.dirname(__file__), 'scraper.log')
logging.basicConfig(filename=filename, level=logging.INFO, format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %I:%M:%S')

def info(message):
    logging.info(message)
