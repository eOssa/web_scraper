import json
import os

class CacheManager():
    filename = os.path.join(os.path.dirname(__file__), 'persistance.json')

    def get(self, key: str, default=None):
        store = self.get_json()
        try:
            return store[key]
        except:
            return default
    
    def has(self, key: str):
        store = self.get_json()
        return key in store

    def put(self, key: str, value):
        store = self.get_json()
        store[key] = value
        self.write_json(store)
        return True

    def forget(self, key: str):
        store = self.get_json()
        del store[key]
        self.write_json(store)
        return True
    
    def flush(self):
        self.write_json({})

    def create_file(self):
        self.write_file('{}')
        return open(self.filename).read()

    def read_file(self):
        try:
            return open(self.filename).read()
        except:
            return self.create_file()
    
    def write_file(self, content):
        handler = open(self.filename, 'w')
        handler.write(content)
        handler.close()

    def get_json(self):
        return json.loads(self.read_file())

    def write_json(self, data):
        self.write_file(json.dumps(data))
