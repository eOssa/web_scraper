import csv
import os
import requests
import sqlite3
import time

from pymongo import MongoClient
from web_scraper import settings

def exists_property_by_reference(reference) -> bool:
    return find_property_by_reference(reference) != None

def exists_word_by_sign(sign) -> bool:
    return find_word_by_sign(sign) != None

def product_was_updated_by_reference(reference: str, updated_at: str) -> bool:
    result = find_property_by_reference(reference)
    if result == None:
        return False
    return result['updated_at'] != updated_at

def find_city_by_name(name):
    return SqliteConnector().find_city_by_name(name)

def find_city(id):
    return SqliteConnector().find_city(id)

def find_mc_city_by_name(name):
    return SqliteConnector().find_mc_city_by_name(name)

def find_region_by_name(name):
    return SqliteConnector().find_region_by_name(name)

def find_property_by_reference(reference):
    return MongoDbConnector().find_property_by_reference(reference)

def find_word_by_sign(sign):
    return MongoDbConnector().find_word_by_sign(sign)

def insert_property(attributes):
    return MongoDbConnector().insert_property(attributes)

def get_all_products_ids() -> list[str]:
    return MongoDbConnector().get_all_products_ids()

def get_all_products_ids_by_serie(serie: str) -> list[str]:
    return MongoDbConnector().get_all_products_ids_by_serie(serie)

def insert_word(attributes):
    return MongoDbConnector().insert_word(attributes)

def update_property_by_id(id, attributes):
    return MongoDbConnector().update_property_by_id(id, attributes)

def update_property_by_reference(reference, attributes):
    return MongoDbConnector().update_property_by_reference(reference, attributes)

def export_properties_to_csv():
    return MongoDbConnector().export_properties_to_csv()

def export_words():
    return MongoDbConnector().export_words()

class MongoDbConnector():
    host = settings.DATABASES['default']['host']
    port = settings.DATABASES['default']['port']
    username = settings.DATABASES['default']['username']
    password = settings.DATABASES['default']['password']
    database = settings.DATABASES['default']['database']
    auth_source = settings.DATABASES['default']['auth_source']

    def find_property_by_reference(self, reference):
        return self.get_default_database().properties.find_one({"reference": reference})

    def find_word_by_sign(self, sign):
        return self.get_default_database().words.find_one({"sign": sign})

    def find_property_by_reference_and_updated_at(self, reference, updated_at):
        return self.get_default_database().properties.find_one({"reference": reference, "updated_at": updated_at})
    
    def get_all_products_ids(self):
        return self.get_default_database().properties.distinct('id')
    
    def get_all_products_ids_by_serie(self, serie: str):
        return self.get_default_database().properties.distinct('id', {'serie': serie})

    def insert_property(self, attributes):
        return self.insert('properties', attributes)

    def insert_word(self, attributes):
        return self.insert('words', attributes)

    def insert(self, collection: str, attributes: dict[str, any]):
        return self.get_default_database()[collection].insert_one(attributes)
    
    def update_property_by_id(self, id: str, attributes):
        return self.get_default_database().properties.update_one({"id": id}, {"$set": attributes})

    def update_property_by_reference(self, reference: str, attributes):
        return self.get_default_database().properties.update_one({"reference": reference}, {"$set": attributes})
    
    def export_properties_to_csv(self):
        db = self.get_default_database()
        path_filename = os.path.join(os.path.dirname(__file__), '..', 'properties.csv')
        with open(path_filename, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, list(db.properties.find_one().keys()))
            writer.writeheader()
            for record in db.properties.find({}):
                writer.writerow(record)

    def export_words(self):
        db = self.get_default_database()
        path_filename = os.path.join(os.path.dirname(__file__), '..', 'export', 'anki', 'LSC Insor Educativo.txt')
        with open(path_filename, 'w', newline='') as csvfile:
            csvfile.writelines(['#separator:tab\n', '#html:true\n'])
            writer = csv.DictWriter(csvfile, ['Seña', 'Imagen Seña', 'Vídeo Seña', 'Definición', 'Ejemplo', 'Vídeo Ejemplo'], delimiter='\t')
            index = 0
            for record in db.words.find({}):
                index += 1
                sign_image = self.download_media(record['sign_image'])
                sign_video = self.download_media(record['sign_video'])
                example_video = self.download_media(record['example_video'])
                writer.writerow({
                    'Seña': record['sign'],
                    'Imagen Seña': '<img src="{}">'.format(sign_image),
                    'Vídeo Seña': '[sound:{}]'.format(sign_video),
                    'Definición': record['meaning'].strip().replace('\n', '').replace('\r', ''),
                    'Ejemplo': record['example'].strip().replace('\n', '<br>').replace('\r', ''),
                    'Vídeo Ejemplo': '[sound:{}]'.format(example_video),
                })
                time.sleep(1)
            writer.writeheader()

    def download_media(self, url: str) -> str:
        filename = url.split('/')[-1]
        path = os.path.join(os.path.dirname(__file__), '..', 'export', 'anki', 'media', filename)
        if not os.path.exists(path):
            with open(path, 'wb') as f:
                f.write(requests.get(url).content)
        return filename

    def connect(self):
        credentials = ''
        auth_source = ''
        if self.username and self.password:
            credentials = f'{self.username}:{self.password}@'
        if credentials:
            auth_source = '?authSource=' + self.auth_source
        return MongoClient(f'mongodb://{credentials}{self.host}:{self.port}/{self.database}{auth_source}')
    
    def get_default_database(self):
        return self.connect().get_default_database()

class SqliteConnector():
    name = os.path.join(os.path.dirname(__file__), '../wasi.sqlite')
    conn = None

    def find_region_by_name(self, name):
        c = self.connect()
        c.execute('SELECT * FROM regions WHERE name like ?', (name,))
        result = c.fetchone()
        self.close()
        return result

    def find_city_by_name(self, name):
        c = self.connect()
        c.execute('SELECT * FROM cities WHERE name like ?', (name,))
        result = c.fetchone()
        self.close()
        return result

    def find_city(self, id):
        c = self.connect()
        c.execute('SELECT * FROM cities WHERE id = ?', (id,))
        result = c.fetchone()
        self.close()
        return result

    def find_mc_city_by_name(self, id):
        c = self.connect()
        c.execute('SELECT * FROM mc_cities WHERE id = ?', (id,))
        result = c.fetchone()
        self.close()
        return result

    def populate(self):
        c = self.connect()
        c.execute('DROP TABLE IF EXISTS regions')
        c.execute('DROP TABLE IF EXISTS cities')
        c.execute('DROP TABLE IF EXISTS mc_cities')
        c.execute("CREATE TABLE regions (id INTEGER PRIMARY KEY, country_id INTEGER, name TEXT)")
        c.execute("CREATE TABLE cities (id INTEGER PRIMARY KEY, region_id INTEGER, name TEXT)")
        c.execute("CREATE TABLE mc_cities (id INTEGER, wasi_city_id INTEGER)")
        with open('regions.csv', newline='') as csvfile:
            reader = csv.reader(csvfile)
            data = []
            for row in reader:
                data.append(tuple(row))
            c.executemany('INSERT INTO regions VALUES (?,?,?)', data)
        with open('cities.csv', newline='') as csvfile:
            reader = csv.reader(csvfile)
            data = []
            for row in reader:
                data.append(tuple(row))
            c.executemany('INSERT INTO cities VALUES (?,?,?)', data)
        with open('mc_cities.csv', newline='') as csvfile:
            reader = csv.reader(csvfile)
            data = []
            for row in reader:
                data.append(tuple(row))
            c.executemany('INSERT INTO mc_cities VALUES (?,?)', data)
        self.persist()
        self.close()

    def connect(self):
        self.conn = sqlite3.connect(self.name)
        return self.conn.cursor()

    def persist(self):
        self.conn.commit()

    def close(self):
        self.conn.close()
