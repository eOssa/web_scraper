import os

from dotenv import load_dotenv

load_dotenv()

BROWSERS = {
    'default': {
        'browser': os.getenv('BROWSER', 'chrome'),
    },
    'chrome': {
        'headless': os.getenv('CHROME_HEADLESS', 'true') == 'true',
    },
    'edge': {
        'headless': os.getenv('EDGE_HEADLESS', 'true') == 'true',
    },
    'firefox': {
        'binary_location': os.getenv('FIREFOX_BINARY_LOCATION'),
        'headless': os.getenv('FIREFOX_HEADLESS', 'true') == 'true',
    },
}
DATABASES = {
    'default': {
        'host': os.getenv('DB_HOST'),
        'port': os.getenv('DB_PORT'),
        'username': os.getenv('DB_USER'),
        'password': os.getenv('DB_PASSWORD'),
        'database': os.getenv('DB_DATABASE'),
        'auth_source': os.getenv('DB_AUTHENTICATION_COLLECTION'),
    }
}
MAIL = {
    'google': {
        'username': os.getenv('MAIL_USERNAME'),
        'password': os.getenv('MAIL_PASSWORD'),
        'from_address': os.getenv('MAIL_FROM_ADDRESS'),
        'to_address': os.getenv('MAIL_TO_ADRRESS'),
    }
}
SERVICES = {
    'visa': {
        'country': os.getenv('VISA_COUNTRY'),
        'email': os.getenv('VISA_EMAIL'),
        'password': os.getenv('VISA_PASSWORD'),
        'schedule_id': os.getenv('VISA_SCHEDULE_ID'),
    }
}
