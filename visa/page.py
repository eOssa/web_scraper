from dusk.browser import Browser

class Page():
    host = 'https://ais.usvisa-info.com'
    browser: Browser

    def __init__(self, browser: Browser):
        self.browser = browser
