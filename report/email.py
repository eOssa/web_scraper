import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from web_scraper import settings

def send(subject, message, to=None):
    transport = GoogleTransport()
    transport.set_subject(subject)
    transport.set_message(message)
    if (to != None):
        transport.set_to(to)
    transport.send()

class GoogleTransport():
    host = 'smtp.googlemail.com'
    port = 465
    username = settings.MAIL['google']['username']
    password = settings.MAIL['google']['password']
    from_address = settings.MAIL['google']['from_address']
    to = settings.MAIL['google']['to_address']
    subject = ''
    message = ''

    def set_to(self, to):
        self.to = to

    def set_subject(self, subject):
        self.subject = subject

    def set_message(self, message):
        self.message = message

    def send(self):
        body = MIMEMultipart('alternative')
        body['subject'] = self.subject
        body['to'] = self.to
        body['from'] = self.from_address
        body.attach(MIMEText(self.message, 'plain'))
        with smtplib.SMTP_SSL(self.host, self.port) as server:
            server.login(self.username, self.password)
            server.sendmail(self.from_address, self.to, body.as_string().encode('ascii'))
            server.quit()
