"""
The factory for create web driver.
"""

from pathlib import Path
import platform

from selenium import webdriver
from web_scraper import settings

class WebDriverFactory:
    """
    The factory for create web driver.
    """

    @staticmethod
    def create_web_driver():
        """
        Create web driver.
        """
        browser: str = settings.BROWSERS['default']['browser']
        if browser == 'chrome':
            options = webdriver.ChromeOptions()
            options.headless = settings.BROWSERS['chrome']['headless']
            options.add_argument('--disable-gpu')
            options.add_argument('--window-size=1920,1080')
            return webdriver.Chrome(
                service=webdriver.ChromeService(
                    executable_path=WebDriverFactory.get_platform_web_driver_name('bin/chromedriver'),
                ),
                options=options,
            )
        if browser == 'firefox':
            options = webdriver.FirefoxOptions()
            options.binary_location = settings.BROWSERS['firefox']['binary_location']
            options.headless = settings.BROWSERS['firefox']['headless']
            return webdriver.Firefox(
                service=webdriver.FirefoxService(
                    executable_path=WebDriverFactory.get_platform_web_driver_name('bin/geckodriver'),
                ),
                options=options,
            )
        if browser == 'safari':
            driver = webdriver.Safari()
            driver.maximize_window()
            return driver
        if browser == 'edge':
            options = webdriver.EdgeOptions()
            options.headless = settings.BROWSERS['edge']['headless']
            options.add_argument('--disable-gpu')
            options.add_argument('--window-size=1920,1080')
            return webdriver.Edge(
                service=webdriver.EdgeService(
                    executable_path=WebDriverFactory.get_platform_web_driver_name('bin/msedgedriver'),
                ),
                options=options
            )
        raise Exception(f'Browser [{browser}] is not supported')

    @staticmethod
    def get_platform_web_driver_name(driver_name: str) -> str:
        """
        Get the driver name based on the platform.
        """
        driver_name = Path(driver_name)
        if platform.system() == 'Windows':
            return f'%s.exe' % driver_name
        return driver_name
