from urllib import request
import json
import ssl

import ciencuadras
from .models import CiencuadrasProperty
from .page import Page

class List(Page):
    path = '/realestates'

    def do_request(self, page=1, filters={}) -> dict[str, int | list]:
        trade_type = filters['trade_type']
        property_type = filters['property_type']
        if (trade_type == None):
            raise Exception('Trade type is required in Ciencuadras')
        criteria = [
            {"transactionType": trade_type},
            {"realEstateType": property_type} if property_type != None else {},
            {},
            {},
            {},
            {"IwantWith": [{"lift": False}, {"childrenArea": False}, {"surveillance": False}, {"deposit": False}, {"communalLiving": False}, {"fitnessCenter": False}, {"swimmingpool": False}]},
            {},
            {"orderBy": {}},
            self.get_sale_price_criteria(trade_type, filters),
            self.get_rent_price_criteria(trade_type, filters),
            {},
            {},
            {},
            {"proyect": 0},
            {},
            {},
            {},
            {},
            {"departmentReal": None},
            {"cityReal": None},
            {"locationReal": None},
            {"neighborhoodReal": None},
            {},
            {"countCityRepeat": 1},
            {"offer": 0},
            {"locationGPS": {}},
            {},
            {},
            {},
            {},
            {},
            {"isSortingFincaRaiz": False},
            {"hasFilter": True},
            {"auction": 0},
            {"sorting": "num_habitaciones"},
        ]
        data = json.dumps({"numberPaginator": page, "pathurl": trade_type, "any": False, "status": False, "criteria": criteria}).encode()
        headers = {
            "Referer": f"https://www.ciencuadras.com/{trade_type}{'/'+property_type if property_type != None else ''}",
            "Content-type": "application/json",
            "Accept": "application/json",
            "User-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36 OPR/70.0.3728.178"
        }
        req = request.Request(self.get_url(), data=data, headers=headers, origin_req_host="https://www.ciencuadras.com", method="POST")
        # Ignore SSL certificate errors
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        response = request.urlopen(req, context=ctx).read().decode()
        return json.loads(response)['data']
    
    def get_data(self, page, filters) -> list[CiencuadrasProperty]:
        data = self.do_request(page=page, filters=filters)
        result = data['result'] + data['highlight']
        properties: list[CiencuadrasProperty] = []
        for property in result:
            properties.append(CiencuadrasProperty(property))
        return properties

    def get_meta(self, filters) -> dict[str, int]:
        data = self.do_request(filters=filters)
        return {'total_pages': data['totalPages'], 'total': data['total']}

    def get_url(self):
        return self.host + self.path

    def get_sale_price_criteria(self, trade_type, filters):
        if trade_type == ciencuadras.SALE and 'price_from' in filters:
            return {"salePrice": {"from": filters['price_from'], "to": filters['price_to']}}
        return {}

    def get_rent_price_criteria(self, trade_type, filters):
        if trade_type == ciencuadras.RENT and 'price_from' in filters:
            return {"rentalPrice": {"from": filters['price_from'], "to": filters['price_to']}}
        return {}
