from datetime import datetime
from persistance import db
from wasi.models import Property, PropertyFeature, PropertyType, TradeType
from model import model

class CiencuadrasProperty(model.Model):
    id = 0
    tipotransaccion = ''
    id_tipo_transaccion = 0
    tipoinmueble = ''
    id_tipo_inmueble = 0
    pais = ''
    id_pais = 0
    departamento = ''
    id_depto = 0
    ciudad = ''
    id_ciudad = 0
    barrio = ''
    id_barrio = 0
    direccion = ''
    area_lote = ''
    area_construida = ''
    valor_administracion = ''
    precio_venta = ''
    canon_arrendamiento = ''
    num_habitaciones = 0
    num_banos = 0
    num_parqueaderos = 0
    estrato = ''
    latitud = ''
    longitud = ''
    antiguedad = ''
    amoblado = ''
    para_estrenar = ''
    activo = ''
    destacado = ''
    fecha_creacion = ''
    fecha_modificacion = ''
    localidad = ''
    id_localidad = 0
    area_privada = ''
    zona_infantil = ''
    gimnasio = ''
    vigilancia = ''
    piscina_comunal = ''
    salon_comunal = ''
    num_ascensores = 0
    vista = ''
    cuarto_servicio = ''
    zona_lavanderia = ''
    num_terraza = 0
    num_balcones = 0
    num_depositos = 0
    num_parqueadero_visita = 0
    recepcion = ''
    circuito_cerrado_tv = ''
    sede_social = ''
    planta_electrica = ''
    zonas_verdes = ''

    def __init__(self, attributes=[]):
        for field in attributes:
            if hasattr(self, field):
                self.__setattr__(field, attributes[field])

    def get_address(self):
        return self.direccion if self.direccion != None else ''

    def get_administration_fee(self):
        return self.valor_administracion if self.area_lote != None else 0

    def get_area(self):
        return str(self.area_lote if self.area_lote != None else '')

    def get_bathrooms(self):
        return str(self.num_banos if self.num_banos != None else '')

    def get_bedrooms(self):
        return str(self.num_habitaciones if self.num_habitaciones != None else '')

    def get_built_area(self):
        return str(self.area_construida if self.area_construida != None else '')

    def get_built_date(self):
        if self.antiguedad == '' or self.antiguedad == 0 or self.antiguedad == None:
            return ''
        return str(datetime.now().year - self.antiguedad)

    def get_city_id(self):
        if self.ciudad == 'Rovira':
            return 694
        if self.ciudad == 'Gachala':
            return 317
        if self.ciudad == 'Gambita':
            return 326
        if self.ciudad == 'Caqueza':
            return 146
        if self.ciudad == 'Sonson':
            return 833
        if self.ciudad == 'Guadalajara De Buga':
            return 111
        if self.ciudad == 'Peñol':
            return 599
        if self.ciudad == 'Chinauta':
            return 316
        if self.ciudad == 'Guatape':
            return 358
        if self.ciudad == 'Municipio Soledad':
            return 831
        if self.ciudad == 'Siberia':
            return 226
        city = db.find_city_by_name(self.ciudad)
        if (city != None):
            return city[0]
        return 0

    def get_condition(self):
        if self.para_estrenar == '0' or self.para_estrenar == '':
            return Property.CONDITION_USED
        return Property.CONDITION_NEW

    def get_country_id(self):
        return self.id_pais

    def get_created_at(self):
        return self.fecha_creacion

    def get_features(self):
        features = []
        if self.amoblado:
            features.append(PropertyFeature.FURNISHED)
        if self.zona_infantil:
            features.append(PropertyFeature.CHILD_AREA)
        if self.gimnasio:
            features.append(PropertyFeature.GYM)
        if self.vigilancia:
            features.append(PropertyFeature.SURVEILLANCE)
        if self.piscina_comunal:
            features.append(PropertyFeature.POOL)
        if self.salon_comunal:
            features.append(PropertyFeature.COMMON_ROOM)
        if self.num_ascensores:
            features.append(PropertyFeature.ELEVATOR)
        if self.vista:
            features.append(PropertyFeature.PANORAMIC_VIEW)
        if self.cuarto_servicio:
            features.append(PropertyFeature.SERVICE_ROOM)
        if self.zona_lavanderia:
            features.append(PropertyFeature.LAUNDRY_AREA)
        if self.num_terraza:
            features.append(PropertyFeature.TERRACE)
        if self.num_balcones:
            features.append(PropertyFeature.BALCONY)
        if self.num_depositos:
            features.append(PropertyFeature.DEPOSIT)
        if self.num_parqueadero_visita:
            features.append(PropertyFeature.PARKING_VISITORS)
        if self.recepcion:
            features.append(PropertyFeature.RECEPTION)
        if self.circuito_cerrado_tv:
            features.append(PropertyFeature.CLOSED_TV_CIRCUIT)
        if self.sede_social:
            features.append(PropertyFeature.SOCIAL_AREA)
        if self.planta_electrica:
            features.append(PropertyFeature.POWER_PLANT)
        if self.zonas_verdes:
            features.append(PropertyFeature.PARKLAND)
        return features

    def get_furnished(self):
        if self.amoblado:
            return 'Si'
        return 'No'

    def get_garages(self):
        return float(self.num_parqueaderos if self.num_parqueaderos != None else 0)

    def get_location_id(self):
        # TODO: DB equivalence
        return float(self.id_localidad)

    def get_map(self):
        if (self.latitud != '' and self.latitud != None and self.longitud != '' and self.longitud != None):
            return '%s,%s' % (self.latitud, self.longitud)
        return ''

    def get_private_area(self):
        return str(self.area_privada if self.area_privada != None else '')
    
    def get_property_model(self) -> Property:
        model = Property()
        model.address = self.get_address()
        model.administration_fee = self.get_administration_fee()
        model.area = self.get_area()
        model.bathrooms = self.get_bathrooms()
        model.bedrooms = self.get_bedrooms()
        model.built_area = self.get_built_area()
        model.built_date = self.get_built_date()
        model.city_id = self.get_city_id()
        model.condition = self.get_condition()
        model.created_at = self.get_created_at()
        model.features = self.get_features()
        model.furnished = self.get_furnished()
        model.garages = self.get_garages()
        model.map = self.get_map()
        model.private_area = self.get_private_area()
        model.rating = self.get_rating()
        model.reference = self.get_reference()
        model.region_id = self.get_region_id()
        model.rent_price = self.get_rent_price()
        model.sale_price = self.get_sale_price()
        model.title = self.get_title()
        model.status = self.get_status()
        model.trade_type_id = self.get_trade_type_id()
        model.type_id = self.get_type_id()
        model.updated_at = self.get_updated_at()
        return model

    def get_rating(self):
        return str(self.estrato if self.area_lote != None else '')
    
    def get_reference(self):
        return 'Ciencuadras:{}'.format(self.id)

    def get_region_id(self):
        region = db.find_region_by_name(self.departamento)
        if (region != None):
            return region[0]
        print('region', self.departamento)
        return 0

    def get_rent_price(self):
        return str(self.canon_arrendamiento if self.canon_arrendamiento != None else '')

    def get_sale_price(self):
        return float(self.precio_venta if self.precio_venta != None else 0)

    def get_status(self):
        if self.destacado:
            return Property.STATUS_FEATURED
        if self.activo == '0':
            return Property.STATUS_ACTIVE
        return Property.STATUS_INACTIVE

    def get_title(self):
        return '{} en {} - {}, {}'.format(self.tipoinmueble, self.tipotransaccion, self.barrio, self.ciudad)

    def get_trade_type_id(self):
        if self.id_tipo_transaccion == 3:
            return TradeType.SALE_RENT
        elif self.id_tipo_transaccion == 2:
            return TradeType.RENT
        else:
            return TradeType.SALE

    def get_type_id(self):
        if self.id_tipo_inmueble == 10:
            return PropertyType.APARTMENT
        elif self.id_tipo_inmueble == 12:
            return PropertyType.ESTATE
        elif self.id_tipo_inmueble == 13:
            return PropertyType.OFFICE
        elif self.id_tipo_inmueble == 14:
            return PropertyType.CONSULTING_ROOM
        elif self.id_tipo_inmueble == 15:
            return PropertyType.WAREHOUSE
        elif self.id_tipo_inmueble == 16:
            return PropertyType.COMMERCIAL_PROPERTY
        elif self.id_tipo_inmueble == 17:
            return PropertyType.LAND
        elif self.id_tipo_inmueble == 21:
            return PropertyType.BUILDING
        elif self.id_tipo_inmueble == 11:
            return PropertyType.HOUSE
        else:
            return PropertyType.HOUSE

    def get_updated_at(self):
        return self.fecha_modificacion

    def get_zone_id(self):
        # TODO: DB equivalence
        return self.id_barrio
