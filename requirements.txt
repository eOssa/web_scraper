beautifulsoup4>=4.0,<5.0
dependency-injector>=4.0,<5.0
pymongo>=4.0,<5.0
python-dotenv>=1.0,<2.0
pytz
schedule>=1.0,<2.0
selenium==4.*
