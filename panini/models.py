from datetime import datetime
from model import model

class PaniniProduct(model.Model):
    id = 0
    title = ''
    handle = ''
    price = ''
    volume = ''
    serie = ''
    option_value = ''
    url = ''
    available = ''
    sku = ''
    published_at = ''
    created_at = ''
    updated_at = ''
    reference = ''

    def __init__(self, attributes=[]):
        for field in attributes:
            if hasattr(self, field):
                self.__setattr__(field, attributes[field])
