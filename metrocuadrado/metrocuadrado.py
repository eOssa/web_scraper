from urllib import request, parse
import json
import math
import ssl

from .models import MetrocuadradoProperty
from .page import Page
from bs4 import BeautifulSoup
import metrocuadrado

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

class List(Page):
    path = '/rest-search/search'
    perPage = 50

    def do_request(self, trade_type, property_type=None, page=1, filters={}) -> dict[str, int | list]:
        if (trade_type == None):
            raise Exception('Trade type is required in Metrocuadrado')
        data = {"from": (page - 1) * self.perPage, "size": self.perPage, "realEstateBusinessList": trade_type, "realEstateTypeList": property_type}
        data = parse.urlencode(data)
        if 'price_from' in filters:
            if trade_type == metrocuadrado.RENT:
                data += f"&leaseRange={int(filters['price_from'])}"
            if trade_type == metrocuadrado.SALE:
                data += f"&saleRange={int(filters['price_from'])}"
        if 'price_to' in filters:
            if trade_type == metrocuadrado.RENT:
                data += f"&leaseRange={int(filters['price_to'])}"
            if trade_type == metrocuadrado.SALE:
                data += f"&saleRange={int(filters['price_to'])}"
        headers = {
            "Referer": f"{self.host}{'/'+property_type if property_type != None else ''}/{trade_type}?search=form",
            "Content-type": "application/json",
            "Accept": "application/json",
            "User-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36 OPR/70.0.3728.178",
            "X-api-key": "P1MfFHfQMOtL16Zpg36NcntJYCLFm8FqFfudnavl",
            "X-requested-with": "XMLHttpRequest",
        }
        req = request.Request(self.get_url()+'?'+data, headers=headers, origin_req_host=self.host, method="GET")
        response = request.urlopen(req, context=ctx).read().decode()
        return json.loads(response)
    
    def get_data(self, page, filters) -> list[MetrocuadradoProperty]:
        data = self.do_request(filters['trade_type'], filters['property_type'], page, filters)
        results = data['results'] + data['highlights']
        properties: list[MetrocuadradoProperty] = []
        for result in results:
            link = f'/inmueble/{filters["trade_type"]}-{filters["property_type"]}/{result["midinmueble"]}'
            print('Getting {}'.format(link))
            properties.append(MetrocuadradoProperty(Detail(link).get_data()))
        return properties

    def get_meta(self, filters) -> dict[str, int]:
        data = self.do_request(filters['trade_type'], filters['property_type'], filters=filters)
        return {'total_pages': math.ceil(data['totalHits'] / self.perPage) + 1, 'total': data['totalHits']}

    def get_url(self):
        return self.host + self.path

class Detail(Page):
    link: str = ''

    def __init__(self, link: str):
        self.link = link
    
    def get_data(self) -> dict[str, any]:
        html = request.urlopen(self.get_url(), context=ctx).read()
        soup = BeautifulSoup(html, 'html.parser')
        return json.loads(soup.select_one('script#__NEXT_DATA__').contents[0])['props']['initialProps']['pageProps']['realEstate']

    def get_url(self):
        return self.host + self.link
