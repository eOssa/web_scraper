import re
from datetime import datetime
from pytz import timezone

import metrocuadrado
from persistance import db
from wasi.models import Property, PropertyFeature, PropertyType, TradeType
from model import model

class MetrocuadradoProperty(model.Model):
    propertyId = ''
    propertyType = {'id': 0, 'nombre': ''}
    businessTypeId = ''
    salePrice = 0
    rentPrice = 0
    area = 0
    areac = 0
    rooms = ''
    bathrooms = ''
    garages = ''
    city = {'id': 0, 'nombre': ''}
    zone = {'id': 0, 'nombre': ''}
    propertyState = ''
    coordinates = {"lon": 0, "lat": 0}
    title = ''
    stratum = ''
    video = ''
    detail = {'adminPrice': 0}
    featured = ''

    def __init__(self, attributes=[]):
        for field in attributes:
            if hasattr(self, field):
                self.__setattr__(field, attributes[field])

    def get_administration_fee(self):
        return self.detail['adminPrice']

    def get_area(self):
        for features_group in self.featured:
            for feature in features_group:
                ocurrences = re.findall('Area lote ([0-9]+) m²', feature)
                if len(ocurrences):
                    return int(ocurrences[0])
        return 0

    def get_bathrooms(self):
        return self.bathrooms

    def get_bedrooms(self):
        return self.rooms

    def get_built_area(self):
        return self.areac

    def get_city(self):
        city = db.find_city(self.get_mc_city()[1])
        if city != None:
            return city
        return 0, 0, ""

    def get_mc_city(self):
        city = db.find_mc_city_by_name(self.city['id'])
        if city != None:
            return city
        return 0, 0

    def get_city_id(self):
        return self.get_city()[0]

    def get_condition(self):
        if self.propertyState == 'Nuevo':
            return Property.CONDITION_NEW
        if self.propertyState == 'Usado':
            return Property.CONDITION_USED
        return Property.CONDITION_USED

    def get_created_at(self):
        return datetime.now(tz=timezone('America/Bogota'))

    def get_features(self):
        features = []
        for features_group in self.featured:
            for feature in features_group['items']:
                if feature == 'Equipado / amoblado':
                    features.append(PropertyFeature.FURNISHED)
                if feature == 'Terraza/Balcón balcón':
                    features.append(PropertyFeature.BALCONY)
                if feature == 'Estudio o biblioteca':
                    features.append(PropertyFeature.LIBRARY_STUDY)
                if feature == 'Cuarto de servicio':
                    features.append(PropertyFeature.SERVICE_ROOM)
                if feature == 'Jacuzzi':
                    features.append(PropertyFeature.JACUZZI)
                if feature == 'Sauna / turco':
                    features.append(PropertyFeature.SAUNA)
                if feature == 'Vista panorámica':
                    features.append(PropertyFeature.PANORAMIC_VIEW)
                if feature == 'Zona de lavandería':
                    features.append(PropertyFeature.LAUNDRY_AREA)
                if feature == 'Cancha(s) de squash':
                    features.append(PropertyFeature.SQUASH_COURT)
                if feature == 'Cancha(s) de tennis':
                    features.append(PropertyFeature.TENNIS_COURT)
                if feature == 'Gimnasio':
                    features.append(PropertyFeature.GYM)
                if feature == 'Jardín':
                    features.append(PropertyFeature.GARDEN)
                if feature == 'Piscina':
                    features.append(PropertyFeature.POOL)
                if feature == 'Zona para niños':
                    features.append(PropertyFeature.CHILD_AREA)
                if feature == 'Zonas verdes':
                    features.append(PropertyFeature.PARKLAND)
                if feature == 'Parqueadero cubierto' or feature == 'Tipo de parqueadero propio':
                    features.append(PropertyFeature.GARAGE)
                if feature == 'Salón comunal':
                    features.append(PropertyFeature.COMMON_ROOM)
                if feature == 'Terraza/Balcón terraza':
                    features.append(PropertyFeature.TERRACE)
                if feature == 'Ascensor':
                    features.append(PropertyFeature.ELEVATOR)
                if feature == 'Conjunto cerrado':
                    features.append(PropertyFeature.CLOSED_URBANIZATION)
                if feature == 'Vigilancia 24hrs':
                    features.append(PropertyFeature.SURVEILLANCE)
                if feature == 'Cancha(s) de basket':
                    features.append(PropertyFeature.BASKETBALL_COURT)
                if feature == 'Cancha(s) de fútbol':
                    features.append(PropertyFeature.SOCCER_FIELD)
                if feature == 'Cerca transporte público':
                    features.append(PropertyFeature.NEAR_PUBLIC_TRANSPORTATION)
                if feature == 'Sobre vía principal':
                    features.append(PropertyFeature.ON_MAJOR_ROAD)
                if feature == 'Citófonos':
                    features.append(PropertyFeature.CITOPHONE)
                if feature == 'Cerca centros comerciales':
                    features.append(PropertyFeature.MALLS)
                if feature == 'Cerca colegios / universidades':
                    features.append(PropertyFeature.COLLEGES_UNIVERSITIES)
                if feature == 'Cerca parques':
                    features.append(PropertyFeature.NEAR_PARKS)
                if feature == 'Instalación de gas natural':
                    features.append(PropertyFeature.HOUSEHOLD_GAS)
                if feature == 'Calentador tiene':
                    features.append(PropertyFeature.HEATER)
                if feature == 'Zona de BBQ':
                    features.append(PropertyFeature.GRILL)
                if 'Deposito' in feature:
                    features.append(PropertyFeature.DEPOSIT)
        return features
    
    def get_floor(self):
        for features_group in self.featured:
            for feature in features_group:
                ocurrences = re.findall('Piso ([0-9]+)', feature)
                if len(ocurrences):
                    return int(ocurrences[0])
        return 0
    
    def get_furnished(self):
        if self.has_feature('Equipado / amoblado'):
            return 'Si'
        return 'No'

    def get_garages(self):
        return self.garages

    def get_map(self):
        if (self.coordinates != None and self.coordinates['lat'] != '' and self.coordinates['lat'] != None and self.coordinates['lon'] != '' and self.coordinates['lon'] != None):
            return '%s,%s' % (self.coordinates['lat'], self.coordinates['lon'])
        return ''

    def get_private_area(self):
        return self.area
    
    def get_property_model(self) -> Property:
        model = Property()
        model.administration_fee = self.get_administration_fee()
        model.area = self.get_area()
        model.bathrooms = self.get_bathrooms()
        model.bedrooms = self.get_bedrooms()
        model.built_area = self.get_built_area()
        model.city_id = self.get_city_id()
        model.condition = self.get_condition()
        model.created_at = self.get_created_at()
        model.features = self.get_features()
        model.floor = self.get_floor()
        model.furnished = self.get_furnished()
        model.garages = self.get_garages()
        model.map = self.get_map()
        model.private_area = self.get_private_area()
        model.rating = self.get_rating()
        model.reference = self.get_reference()
        model.region_id = self.get_region_id()
        model.rent_price = self.get_rent_price()
        model.sale_price = self.get_sale_price()
        model.title = self.get_title()
        model.trade_type_id = self.get_trade_type_id()
        model.type_id = self.get_type_id()
        model.updated_at = self.get_updated_at()
        model.video = self.get_video()
        return model

    def get_rating(self):
        return self.stratum
    
    def get_reference(self):
        return f'Metrocuadrado:{self.propertyId}'

    def get_region_id(self):
        return self.get_city()[1]

    def get_rent_price(self):
        return self.rentPrice

    def get_sale_price(self):
        return self.salePrice

    def get_title(self):
        return self.title

    def get_trade_type_id(self):
        if int(self.businessTypeId) == 2:
            return TradeType.RENT
        else:
            return TradeType.SALE

    def get_type_id(self):
        if self.has_feature('Tipo de casa condominio'):
            return PropertyType.CONDOMINIUM
        type_id = int(self.propertyType['id'])
        if type_id == metrocuadrado.OFFICES_BUILDING:
            return PropertyType.BUILDING
        if type_id == metrocuadrado.APARTMENTS_BUILDING:
            return PropertyType.BUILDING
        if type_id == metrocuadrado.WAREHOUSE:
            return PropertyType.WAREHOUSE
        if type_id == metrocuadrado.ESTATE:
            return PropertyType.ESTATE
        if type_id == metrocuadrado.COMMERCIAL_PROPERTY:
            return PropertyType.COMMERCIAL_PROPERTY
        if type_id == metrocuadrado.CONSULTING_ROOM:
            return PropertyType.CONSULTING_ROOM
        if type_id == metrocuadrado.LAND:
            return PropertyType.LAND
        if type_id == metrocuadrado.OFFICE:
            return PropertyType.OFFICE
        if type_id == metrocuadrado.HOUSE:
            return PropertyType.HOUSE
        if type_id == metrocuadrado.APARTMENT:
            return PropertyType.APARTMENT
        return PropertyType.HOUSE

    def get_updated_at(self):
        return datetime.now(tz=timezone('America/Bogota'))
    
    def get_video(self):
        return self.video
    
    def has_feature(self, key):
        for features_group in self.featured:
            for feature in features_group['items']:
                if feature == key:
                    return True
