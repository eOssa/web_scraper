"""
The browser module contains the Browser class, which is used to
interact with the browser.
"""
from typing import Callable
from dusk.element_resolver import ElementResolver
from dusk.mixins.interacts_with_elements import InteractWithElementsMixin
from dusk.mixins.waits_for_elements import WaitForElementMixin

class Browser(InteractWithElementsMixin, WaitForElementMixin):
    """
    The Browser class is the main entry point for Dusk. It is a wrapper around
    the Selenium WebDriver. It is responsible for managing the browser,
    and providing a high-level API for interacting with the browser.
    """
    def __init__(self, driver, resolver=None):
        self.driver = driver
        self.resolver = resolver or ElementResolver(driver)

    def quit(self):
        """
        Quit the browser.
        """
        self.driver.quit()

    def visit(self, page):
        """
        Visit a page.
        """
        self.driver.get(page.url)
        if hasattr(page, 'do_assert'):
            page.do_assert()
        return page

    def within(self, selector: str, callback: Callable):
        """
        Execute a callback within the context of a given selector.
        """
        resolver = ElementResolver(self.driver, self.resolver.format(selector))
        browser = Browser(self.driver, resolver=resolver)
        callback(browser)
