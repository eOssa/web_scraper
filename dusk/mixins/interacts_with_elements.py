"""
Mixin for interacting with elements.
"""

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.ui import Select

from dusk.element_resolver import ElementResolver


class InteractWithElementsMixin:
    """
    Mixin for interacting with elements.
    """

    driver: WebDriver
    resolver: ElementResolver

    def check(self, selector: str, value=None):
        """
        Check a checkbox or radio button.
        """
        element = self.resolver.resolve_for_checking(selector, value)
        if ~element.is_selected():
            element.click()

    def click(self, selector: str):
        """
        Click on an element.
        """
        element = self.resolver.find_by_selector(selector)
        element.click()

    def click_link(self, link: str):
        """
        Click on a link.
        """
        element = self.resolver.find_link_element(link)
        element.click()

    def is_visible(self, selector: str) -> bool:
        """
        Check if an element is visible.
        """
        element = self.resolver.find_by_selector(selector)
        return element.is_displayed()

    def press(self, selector: str):
        """
        Press on a button.
        """
        self.click(selector)

    def select(self, field: str, value: str):
        """
        Select an option from a select field.
        """
        select = Select(self.resolver.resolve_for_selection(field))
        select.select_by_value(value)

    def select_by_index(self, field: str, index: int):
        """
        Select an option by index from a select field.
        """
        select = Select(self.resolver.resolve_for_selection(field))
        select.select_by_index(index)

    def select_by_text(self, field: str, value: str):
        """
        Select an option by text from a select field.
        """
        select = Select(self.resolver.resolve_for_selection(field))
        select.select_by_visible_text(value)

    def type(self, field: str, value: str):
        """
        Type into a field.
        """
        element = self.resolver.resolve_for_typing(field)
        element.clear()
        element.send_keys(value)

    def uncheck(self, selector: str, value):
        """
        Uncheck a checkbox or radio button.
        """
        element = self.resolver.resolve_for_checking(selector, value)
        if element.is_selected():
            element.click()
